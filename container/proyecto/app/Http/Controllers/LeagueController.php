<?php

namespace App\Http\Controllers;

use App\Models\League;
use Error;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LeagueController extends Controller
{
    // Cada usuario puede ver las ligas/liga, crearla, editarla y eliminarla
    // League (league_id, user_id, nombre, num_jornadas)
    // // Pertenecen a USERS
    // // Tienen EQUIPOS

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $league = League::all();
            return new Response(['message'=> 'Datos encontrados', 'elemento' => $league], 200);
        } catch(Error $er){
            return new Response(['message'=> 'No se pudo obtener'], 500);
        } catch(Exception $ex){
            return new Response(['message'=> 'No se pudo obtener'], 400);
        }
    }
    
    /**
     * Display a listing of the resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // if(!is_numeric($id)){
        //     return new Response(['message'=> 'El id tiene que se numerico'], 400);
        // }
        try{
            $league = League::query()->where('id', $id)->get();
            return new Response(['message'=> 'Datos encontrados', 'elemento' => $league], 200);
        } catch(Error $er){
            return new Response(['message'=> 'No se pudo obtener', 'id' => $id], 500);
        } catch(Exception $ex){
            return new Response(['message'=> 'No se pudo obtener', 'id' => $id], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // League (league_id, user_id, nombre, num_jornadas = 0)

        // Que esten puestos
        if(!isset($request->nombre) || !isset($request->user_id)){
            return new Response(['message'=> 'Tiene que incluir el id del usuario creador y el nombre de la liga'], 400);
        }
        // // Datos legibles
        // if(!is_numeric($request->user_id)){
        //     return new Response(['message'=> 'El id tiene que se numerico'], 400);
        // }

        try{
            $league = new League();
            $league->user_id = $request->user_id;
            $league->nombre = $request->nombre;
            $league->num_jornadas = 0;
            $league->save();
            return new Response(['message'=> 'Creado exitosamente', 'elemento'=> $league], 201);
        } catch(Error $er){
            $response = new Response(['message'=> 'No creado'], 500);
        } catch(Exception $ex){
            $response = new Response(['message'=> 'No creado'], 400);
        }
        return $response;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // League (league_id, user_id, nombre, num_jornadas = 0)
        // Solo puede modificar el nombre 
        if(!isset($request->nombre)){
            return new Response(['message'=> 'Solo puede modificar el nombre', 'id'=> $id], 400);
        }
        try{
            $league = League::findOrFail($id);
            $league->nombre = $request->nombre;
            $league->save();
            $response = new Response(['message'=> 'Modificado exitosamente', 'elemento'=> $league], 201);
        } catch(Error $er){
            $response = new Response(['message'=> 'No modificado', 'id'=> $id], 500);
        } catch(Exception $ex){
            $response = new Response(['message'=> 'No modificado', 'id'=> $id], 400);
        }
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // League (league_id, user_id, nombre, num_jornadas = 0)
        try{
            $league = League::findOrFail($id);
            $league->destroy();
            $response = new Response(['message'=> 'Eliminado correctamente', 'id'=> $id], 204);
        } catch(Error $er){
            $response = new Response(['message'=> 'No eliminado', 'id'=> $id], 500);
        } catch(Exception $ex){
            $response = new Response(['message'=> 'No eliminado', 'id'=> $id], 400);
        }
        return $response;
    }
}
