<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeagueTeam extends Model
{
    use HasFactory;
    // league-team (league_id, team_id, puntos_totales, puntos_ultima_jornada, total_jornadas)

    protected $fillable = [
        'team_id',
        'puntos_totales',
        'puntos_ultima_jornada',
        'total_jornadas',
    ];

}
