<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    use HasFactory;

    // Team (team_id, user_id, nombre, flag_12_jugadores, dinero_disponible)
    // // Tienen JUGADORES (4G-4F-2C-2Extras-> Sino penalizacion por roster sin recambios)
    // // Pertenecen a LIGAS

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'user_id',
        'nombre',
        'flag_12_jugadores',
        'dinero_disponible',
    ];

    // Cada Equipo puede tiene varios jugadores
    public function players(){
        return $this->hasMany(Player::class);
    }

    // Cada equipo pertenece a 1 - 3 ligas
    public function leagues(){
        return $this->belongsToMany(League::class);
    }
}
