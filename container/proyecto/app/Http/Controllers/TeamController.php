<?php

namespace App\Http\Controllers;

use App\Models\Team;
use Error;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TeamController extends Controller
{

    // Team (team_id, user_id, nombre, flag_12_jugadores, dinero_disponible)
    // // Tienen JUGADORES (4G-4F-2C-2Extras-> Sino penalizacion por roster sin recambios)
    // // Pertenecen a LIGAS

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
            try{
                $team = Team::all();
                return new Response(['message'=> 'Datos encontrados', 'elemento' => $team], 200);
            } catch(Error $er){
                return new Response(['message'=> 'No se pudo obtener'], 500);
            } catch(Exception $ex){
                return new Response(['message'=> 'No se pudo obtener'], 400);
            }

    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $team = Team::query()->where('id', $id)->get();
            return new Response(['message'=> 'Datos encontrados', 'elemento' => $team], 200);
        } catch(Error $er){
            return new Response(['message'=> 'No se pudo obtener', 'id' => $id], 500);
        } catch(Exception $ex){
            return new Response(['message'=> 'No se pudo obtener', 'id' => $id], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Team (team_id, user_id, nombre, flag_12_jugadores, dinero_disponible)

        // Que esten puestos
        if(!isset($request->user_id) || !isset($request->nombre)){
            return new Response(['message'=> 'Tiene que incluir el id del usuario creador y el nombre del equipo'], 400);
        }

        try{
            $team = new Team();
            $team->user_id = $request->user_id;
            $team->nombre = $request->nombre;
            $team->flag_12_jugadores = false;
            $team->dinero_disponible = 130000000;
            $team->save();
            return new Response(['message'=> 'Creado exitosamente', 'elemento'=> $team], 201);
        } catch(Error $er){
            $response = new Response(['message'=> 'No creado'], 500);
        } catch(Exception $ex){
            $response = new Response(['message'=> 'No creado'], 400);
        }
        return $response;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Team (team_id, user_id, nombre, flag_12_jugadores, dinero_disponible)
        if(!isset($request->nombre)){
            return new Response(['message'=> 'Solo se puede modificar el nombre', 'id'=> $id], 400);
        }
        try{
            $team = Team::findOrFail($id);
            $team->nombre = $request->nombre;
            $team->save();
            $response = new Response(['message'=> 'Modificado exitosamente', 'elemento'=> $team], 201);
        } catch(Error $er){
            $response = new Response(['message'=> 'No modificado', 'id'=> $id], 500);
        } catch(Exception $ex){
            $response = new Response(['message'=> 'No modificado', 'id'=> $id], 400);
        }
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Team (team_id, user_id, nombre, flag_12_jugadores, dinero_disponible)
        try{
            $team = Team::findOrFail($id);
            $team->destroy();
            $response = new Response(['message'=> 'Eliminado correctamente', 'id'=> $id], 204);
        } catch(Error $er){
            $response = new Response(['message'=> 'No eliminado', 'id'=> $id], 500);
        } catch(Exception $ex){
            $response = new Response(['message'=> 'No eliminado', 'id'=> $id], 400);
        }
        return $response;
    }
}
