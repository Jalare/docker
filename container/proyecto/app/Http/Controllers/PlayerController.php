<?php

namespace App\Http\Controllers;

use App\Models\Player;
use Error;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PlayerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Array
     */
    public function index()
    {
        try{
            $player = Player::all();
            return new Response(['message'=> 'Datos encontrados', 'elemento' => $player], 200);
        } catch(Error $er){
            return new Response(['message'=> 'No se pudo obtener'], 500);
        } catch(Exception $ex){
            return new Response(['message'=> 'No se pudo obtener'], 400);
        }

    }
    
    /**
     * Display a listing of the resource.
     *
     * @return App\Models\Player
     */
    public function show($id)
    {
        try{
            $player = Player::query()->where('id', $id)->get();
            return new Response(['message'=> 'Datos encontrados', 'elemento' => $player], 200);
        } catch(Error $er){
            return new Response(['message'=> 'No se pudo obtener', 'id' => $id], 500);
        } catch(Exception $ex){
            return new Response(['message'=> 'No se pudo obtener', 'id' => $id], 400);
        }
    }

}
