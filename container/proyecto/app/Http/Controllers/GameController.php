<?php

namespace App\Http\Controllers;

use App\Models\Game;
use Error;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class GameController extends Controller
{
    // Los usuarios no crean los partidos con las estadisticas de cada jugador

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $game = Game::all();
            return new Response(['message'=> 'Datos encontrados', 'elemento' => $game], 200);
        } catch(Error $er){
            return new Response(['message'=> 'No se pudo obtener'], 500);
        } catch(Exception $ex){
            return new Response(['message'=> 'No se pudo obtener'], 400);
        }

    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $game = Game::query()->where('id', $id)->get();
            return new Response(['message'=> 'Datos encontrados', 'elemento' => $game], 200);
        } catch(Error $er){
            return new Response(['message'=> 'No se pudo obtener'], 500);
        } catch(Exception $ex){
            return new Response(['message'=> 'No se pudo obtener'], 400);
        }
    }
}
